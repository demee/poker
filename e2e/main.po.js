/**
 * This file uses the Page Object pattern to define the main page for tests
 * https://docs.google.com/presentation/d/1B6manhG0zEXkC-H-tPo2vwU06JhL8w9-XCF9oehXzAQ
 */

'use strict';

var MainPage = function() {
  this.loginBtn = element(by.buttonText('Login or Register'));
  this.userName = element(by.css('ul.navbar-right li .navbar-text'));
  
  this.logoutBtn = element(by.css('a[ng-click="logout()"]'));
};

module.exports = new MainPage();
