'use strict';

var Login = function () {
  this.loginInput = element(by.binding('user.email'));
  this.paswordInput = element(by.binding('user.password'));
  this.registerButton = element(by.css('a[href="/signup"].btn'));

  this.connectWithGoogleBtn = element(by.css('.btn-google-plus'));
}

module.exports = new Login();
