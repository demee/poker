/* global describe, beforeEach, afterEach, it, browser, expect */
'use strict';

var Chance = require('chance'),
    chance = new Chance(),
    mainPage = require('../main.po'),
    loginPage = require('./login.po'),
    registerPage = require('./register.po'),
    user = {
      name: chance.name(),
      email: chance.email(),
      password: chance.string()
    };
  

describe('Registration process', function () {
  
  beforeEach(function (done) {
    browser.get('/').then(done);
  })
  
  it('should register new user', function (done) {
    mainPage.loginBtn.click().
    then(function () {
      loginPage.registerButton.click().
      then(function () {
        registerPage.setUserDetails(user);
        registerPage.signUpButton.click().
        then(function () {
          expect(mainPage.userName.getText()).
          toEqual('Hello ' + user.name);
          done();
        }); 
      });  
    });
  });
  
  afterEach(function (done) {
    mainPage.logoutBtn.click().
    then(done);
  });
  
});

