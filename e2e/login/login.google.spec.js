/* global describe, it, browser, beforeEach, afterEach, expect */
'use strict';

var mainPage = require('../main.po'),
    loginPage = require('./login.po'),
    googlePage = require('./google/google.po');
    
describe('Main View', function() {
 beforeEach(function(done) {
    browser.get('/').
    then(done);
  });

  it('shold login though google', function(done) {
    mainPage.loginBtn.click().
    then(function () {
      loginPage.connectWithGoogleBtn.click();
      googlePage.loginToGoogle();
      browser.wait(mainPage.userName.isPresent()).
      then(function () {
        expect(mainPage.userName.getText()).
        toEqual('Hello Testing Testing');
        done();
      });
    });
  });
  
  afterEach(function (done) {
    mainPage.logoutBtn.click().
    then(done);
  });

});
