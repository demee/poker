/* global element, by */ 
'use strict';

var RegisterPage = function () {
	this.userName = element(by.model('user.name'));
	this.userEmail = element(by.model('user.email'));
	this.userPassword = element(by.model('user.password'));
	
	this.signUpButton = element(by.buttonText('Sign up'));
	
	this.setUserDetails = function (user) {
		this.userName.sendKeys(user.name);
        this.userEmail.sendKeys(user.email);
        this.userPassword.sendKeys(user.password);
	}
}

module.exports = new RegisterPage();