'use strict';

angular.module('pokerApp')
  .controller('MainCtrl', function ($scope, $http, $location, socket, Auth) {
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.startSession = function () {
      $http.post('/api/sessions').success(function (session) {
        $scope.session = session;
      });
    };
    
    //$scope.awesomeThings = [];

    /*$http.get('/api/things').success(function(awesomeThings) {
      $scope.awesomeThings = awesomeThings;
      socket.syncUpdates('thing', $scope.awesomeThings);
    });*/

    /* $scope.addThing = function() {
      if($scope.newThing === '') {
        return;
      }
      $http.post('/api/things', { name: $scope.newThing });
      $scope.newThing = '';
    };

    $scope.deleteThing = function(thing) {
      $http.delete('/api/things/' + thing._id);
    }; */ 
    
    

    /*$scope.$on('$destroy', function () {
      socket.unsyncUpdates('thing');
    });*/
  });
